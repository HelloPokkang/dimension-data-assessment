# Assessment 1
def _FirstFactorial(num):
    total = 1
    for i in range(num):
        total = total*(i+1)
    return total

# Assessment 2
def _FirstReverse(s):
    return s[::-1]

# Assessment 3
def _AlphabetSoup(s):
    chars = [c for c in s]
    chars.sort()
    return ''.join(chars)

# Assessment 4
from flask import Flask, request, jsonify
app = Flask(__name__)

'''
The sample endpoint is http://localhost/api/rirstreverse/
but I assumed that I needed to change it to http://localhost/api/firstreverse/ na ka
'''
@app.route("/api/firstfactorial/", methods=['POST'])
def first_factotial():
    input_data = request.get_json()['input']
    app.logger.debug(_FirstFactorial(input_data))
    return jsonify(dict(output=_FirstFactorial(input_data)))


@app.route("/api/firstreverse/", methods=['POST'])
def first_reverse():
    input_data = request.get_json()['input']
    return jsonify(dict(output=_FirstReverse(input_data)))


@app.route("/api/alphabetsoup/", methods=['POST'])
def alphabet_soup():
    input_data = request.get_json()['input']
    return jsonify(dict(output=_AlphabetSoup(input_data)))
if __name__ == '__main__':
     app.run()
